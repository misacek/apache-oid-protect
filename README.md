
This project allows you to protect given directory with Google OpenID for
authentication and apache with mod_auth_openidc for authorization using user
emails all this in ssl host. 

It is intended as a proof of concept that already works and is easy to extend.

Prerequisities:
    * installed and running docker
    * docker-compose installed
    * for google authenticaiton to work you need to go google developers console, enable google+ api, create a project and create oauth 2.0 client credentials and put 'https://localhost:8443/<your directory>/index.html' to Authorized Redirect URIs.
    (check https://console.developers.google.com/apis/dashboard [10/2017])

To run:
    * check .env content (PROJECT_DATA_DIR, PROJECT_OID_USER_EMAIL)
    * ``docker-compose up``


You might want to check that the following works:

    As non-logged user:
        pass: https://localhost:8443
        pass: https://localhost:8443/
        forbidden: https://localhost:8443/oid-protected/index.html
        forbidden: https://localhost:8443/oid-protected/top-secret/inde.html

    As PROJECT_OID_USER_EMAIL:
        fail: https://localhost:8433/oid-protected/top-secret/index.html
        pass: https://localhost:8433/oid-protected/index.html

    As PROJECT_OID_USER2_EMAIL:
        pass: https://localhost:8433/oid-protected/top-secret/index.html
        pass: https://localhost:8433/oid-protected/index.html
