#!/bin/bash -x
trap 'exit 1' ERR

CURL='curl -s -o /dev/null -w %{http_code} --insecure'

[ $($CURL http://localhost:8443) == '400' ]
[ $($CURL https://localhost:8443) == '200' ]
[ $($CURL https://localhost:8443/oid-protected/) == '200' ]
[ $($CURL https://localhost:8443/oid-protected/index.html) == '302' ]

echo Success.
