#!/bin/bash

function log(){
    local MY_NAME=${MY_NAME:-$0}
    logger -t ${MY_NAME} -- $@
    echo "$(date) ${MY_NAME}: $@"
}


function trap_exit(){
    log "Trapped error on line $(caller)"
    log "result: failure"

    tail /var/log/apache2/error*
    exit 1
}

# apache config adjustement
function apache_oid_config(){
    export MY_NAME=${FUNCNAME[0]}
    trap "trap_exit ${MY_NAME}" ERR

    [ -n "${APACHE_OID_CONFIG}" ]
    [ -n "${GOOGLE_OID_SECRET}" ]
    [ -n "${GOOGLE_OID_ID}" ]
    [ -n "${GOOGLE_REDIRECT_URL}" ]

    [ -w ${APACHE_OID_CONFIG} ];

    # because we do not run as root and sed needs temp file at the location of
    # original file
    local TMP_CONFIG=/tmp/$(basename ${APACHE_OID_CONFIG})-$RANDOM$RANDOM
    cp ${APACHE_OID_CONFIG} ${TMP_CONFIG}

    sed -i "s;\$GOOGLE_OID_SECRET;${GOOGLE_OID_SECRET};" ${TMP_CONFIG}
    sed -i "s;\$GOOGLE_OID_ID;${GOOGLE_OID_ID};" ${TMP_CONFIG}
    sed -i "s;\$GOOGLE_REDIRECT_URL;${GOOGLE_REDIRECT_URL};" ${TMP_CONFIG}
    sed -i "s;\$GOOGLE_CRYPTO_PASSPHRASE;${GOOGLE_CRYPTO_PASSPHRASE};" ${TMP_CONFIG}
    sed -i "s;\$APACHE_PROTECTED_PATH;${APACHE_PROTECTED_PATH};" ${TMP_CONFIG}

    cat ${TMP_CONFIG} > ${APACHE_OID_CONFIG}
    if [ -n "$SHOW_CONFIGURATION" ];
    then
        echo -e "----\nfile: ${APACHE_OID_CONFIG}\n---"
        grep -v \# $APACHE_OID_CONFIG | uniq
        echo "#---| end |---#"
    fi
    rm -f ${TMP_CONFIG}

    log "success"
}

# replace ports to be >1024
function replace_ports(){
    export MY_NAME=${FUNCNAME[0]}
    trap "trap_exit ${MY_NAME}" ERR

    [ -n "${APACHE_PORT_CONFIG}" ]
    [ -n "${APACHE_LISTEN_PORT}" ]
    [ -n "${APACHE_LISTEN_PORT_SSL}" ]

    [ -w ${APACHE_OID_CONFIG} ];

    # because we do not run as root and sed needs temp file at the location of
    # original file
    local TMP_CONFIG=/tmp/$(basename ${APACHE_PORT_CONFIG})-$RANDOM$RANDOM
    cp ${APACHE_PORT_CONFIG} ${TMP_CONFIG}

    sed -i "s;Listen 80.*;Listen ${APACHE_LISTEN_PORT};" ${TMP_CONFIG}
    sed -i "s;Listen 443.*;Listen ${APACHE_LISTEN_PORT_SSL};" ${TMP_CONFIG}

    cat ${TMP_CONFIG} > ${APACHE_PORT_CONFIG}
    if [ -n "$SHOW_CONFIGURATION" ];
    then
        echo -e "----\nfile: ${APACHE_PORT_CONFIG}\n----"
        grep -v \# ${APACHE_PORT_CONFIG} | uniq
        echo "#---| end |---#"
    fi
    rm -f ${TMP_CONFIG}

    log "success"
}

function htaccess_config(){
    export MY_NAME=${FUNCNAME[0]}
    trap "trap_exit ${MY_NAME}" ERR

    [ -w "${APACHE_PROTECTED_PATH}" ]

    CONFIG=(
        "\nAuthType openid-connect"
        "\n<RequireAll>"
        "\n\tRequire valid-user"
        "\n\t<RequireAny>"
        "\n\t\tRequire claim email:$APACHE_OID_USER_EMAIL"
        "\n\t\tRequire claim email:$APACHE_OID_USER2_EMAIL"
       "\n\t</RequireAny>"
        "\n</RequireAll>"
    )
    CONFIG_TOP_SECRET=(
        "\nAuthType openid-connect"
        "\n<RequireAll>"
        "\n\tRequire valid-user"
        "\n\t<RequireAny>"
        "\n\t\tRequire claim email:$APACHE_OID_USER_EMAIL"
        "\n\t</RequireAny>"
        "\n</RequireAll>"
    )
    echo -e ${CONFIG[*]} > ${APACHE_PROTECTED_PATH}/.htaccess
    [ -f ${APACHE_PROTECTED_PATH}/index.html ] || echo whee! > ${APACHE_PROTECTED_PATH}/index.html

    mkdir -p ${APACHE_PROTECTED_PATH}/top-secret
    echo -e ${CONFIG_TOP_SECRET[*]} > ${APACHE_PROTECTED_PATH}/top-secret/.htaccess
    echo 'whee!' > $APACHE_PROTECTED_PATH/top-secret/index.html
    [ -f ${APACHE_PROTECTED_PATH}/top-secret/index.html ] || \
        echo whee! > ${APACHE_PROTECTED_PATH}/top-secret/index.html

    if [ -n "$SHOW_CONFIGURATION" ];
    then
        echo -e "----\nfile: ${APACHE_PROTECTED_PATH}/.htaccess\n----"
        grep -v \#  ${APACHE_PROTECTED_PATH}/.htaccess | uniq
        echo "#---| end |---#"

        echo -e "----\nfile: ${APACHE_PROTECTED_PATH}/top-secret/.htaccess\n----"
        grep -v \#  ${APACHE_PROTECTED_PATH}/top-secret/.htaccess | uniq
        echo "#---| end |---#"
    fi
    rm -f ${TMP_CONFIG}

    log "success"
}

trap trap_exit ERR
[ -n "$DEBUG" ] && set -x

export MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir

export GOOGLE_OID_SECRET=${GOOGLE_OID_SECRET:-'not-supposed-to-work'}
export GOOGLE_OID_ID=${GOOGLE_OID_ID:-'not-really-a-secret'}
export GOOGLE_REDIRECT_URL
export GOOGLE_CRYPTO_PASSPHRASE=${GOOGLE_CRYPTO_PASSPHRASE:-$RANDOM$RANDOM$RANDOM}

export APACHE_CONFIG_DIR=${APACHE_CONFIG_DIR:-'/etc/apache2'}
export APACHE_LISTEN_PORT=${APACHE_LISTEN_PORT:-'8080'}
export APACHE_PROTECTED_PATH

export APACHE_LISTEN_PORT_SSL=${APACHE_LISTEN_PORT_SSL:-'5443'}
export APACHE_CERTIFICATE=/etc/apache2/certs/cert.pem
export APACHE_PRIVATE_KEY=/etc/apache2/certs/private-key.pem

export APACHE_OID_USER_EMAIL=${APACHE_OID_USER_EMAIL:-'el-user@oid.example'}
export APACHE_OID_USER2_EMAIL=${APACHE_OID_USER2_EMAIL:-'el-user2@another-oid.example'}

# ----

export SHOW_CONFIGURATION
export APACHE_OID_CONFIG="${APACHE_CONFIG_DIR}/conf-available/oid-protected-pages.conf"
export APACHE_PORT_CONFIG=${APACHE_PORT_CONFIG:-"${APACHE_CONFIG_DIR}/ports.conf"}
export APACHE_SSL_CONFIG="${APACHE_CONFIG_DIR}/sites-available/default-ssl.conf"

export APACHECTL="apache2ctl -d ${APACHE_CONFIG_DIR} -f ${APACHE_CONFIG_DIR}/apache2.conf"
export TMPDIR=/tmp

apache_oid_config
replace_ports
htaccess_config

# syntax check
[ -n "$DEBUG" ] && ${APACHECTL} -tSM

# foreground, errors to file
if [ -n "$DEBUG" ];
then
    # one fork only
    ${APACHECTL} -X -e debug
else
    ${APACHECTL} -DFOREGROUND -e info
fi


