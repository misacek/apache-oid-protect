FROM ubuntu:16.04

ARG BUILD_DATE=1
ARG BUILD_VERSION=unknown

ARG APACHE_CONFIG_DIR=/etc/apache2
ARG APACHE_DOCROOT=/var/www/html
ARG APACHE_PROTECTED_PATH=/var/www/html/oid-protected
ARG APACHE_USER=www-data
ARG APACHE_USER_UID=5001

LABEL maintainer="Michal Novacek <michal.novacek@gmail.com>"
LABEL description="\
Dockerfile used to build this image is here: \
https://gitlab.com/misacek/apache-oid-protect/blob/master/Dockerfile \
"
LABEL build_version="$BUILD_VERSION"
LABEL build_date="$BUILD_DATE"

#HTTPD_PREFIX comes from httpd:2.4 Dockerfile
RUN \
    apt-get update && \
    apt-get -y upgrade && \
    usermod --uid ${APACHE_USER_UID} --gid 0 ${APACHE_USER} && \
    apt-get -y install \
        --no-install-recommends \
        apache2 libapache2-mod-auth-openidc ca-certificates vim && \
    a2enmod auth_openidc && \
    mkdir -p \
        $APACHE_PROTECTED_PATH \
        $APACHE_PROTECTED_PATH/top-secret \
       /var/run/apache2 && \
    chown -R ${APACHE_USER_UID}:root \
        $APACHE_PROTECTED_PATH \
        $APACHE_CONFIG_DIR/ports.conf \
        /var/log/apache2 \
        /var/run/apache2 && \
    sed -i 's;ErrorLog.*;ErrorLog /dev/stdout;' $APACHE_CONFIG_DIR/apache2.conf && \
    sed -i 's;TransferLog.*;TransferLog /dev/stdout;' $APACHE_CONFIG_DIR/apache2.conf

COPY files/oid-protected-pages.conf $APACHE_CONFIG_DIR/conf-available/
COPY files/default-ssl.conf $APACHE_CONFIG_DIR/sites-available/
COPY files/entrypoint.sh /
RUN \
    chmod 755 /entrypoint.sh && \
    chown -R $APACHE_USER_UID:root \
        $APACHE_CONFIG_DIR/conf-available/oid-protected-pages.conf \
        $APACHE_CONFIG_DIR/sites-available/default-ssl.conf && \
    a2enconf oid-protected-pages.conf && \
    a2enmod ssl && \
    a2ensite default-ssl

USER ${APACHE_USER_UID}
WORKDIR $APACHE_PROTECTED_PATH

ENTRYPOINT ["/entrypoint.sh"]
